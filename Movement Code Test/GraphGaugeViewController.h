//
//  FirstViewController.h
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/13/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphView.h"
#import "SharedPubnubConnection.h"

@interface GraphGaugeViewController : UIViewController <UIAlertViewDelegate>

@end

