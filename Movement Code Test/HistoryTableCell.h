//
//  HistoryTableCell.h
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/17/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *historyCellLabel;

@end
