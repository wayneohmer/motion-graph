//
//  GraphView.h
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/14/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

//used in both GraphView and GraphGaugeViewController
NSInteger static maxAcceleration = 10;

@interface GraphView : UIView

- (void)addAccelerationPoint:(CMDeviceMotion *)motionData;
//clear and reInitializeGraph used when view is rotated. Clear on willRotate, reinitialize on didRotate
- (void)clearGraph;
- (void)reinitializeGraph;

@end
