//
//  sharedPubnubChannel.m
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/17/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import "SharedPubnubConnection.h"

NSString* const PubhubOrigin = @"pubsub.pubnub.com";
NSString* const PubhubPublishKeyKey = @"pub-c-1b7c7317-adf0-4eb7-ad6d-adfc5be887a5";
NSString* const PubhubSubscribeKey = @"sub-c-94cd1d5e-9dc1-11e4-80d5-0619f8945a4f";
NSString* const PubhubChannelName = @"Movement_Data";

@implementation SharedPubnubConnection

- (instancetype)init {
    self = [super init];
    if (self){
        PNConfiguration *myConfig = [PNConfiguration configurationForOrigin:PubhubOrigin publishKey:PubhubPublishKeyKey subscribeKey:PubhubSubscribeKey secretKey:nil];
        [PubNub setConfiguration:myConfig];
        [PubNub connect];
        _channel = [PNChannel channelWithName:PubhubChannelName shouldObservePresence:YES];
        [PubNub subscribeOn:@[_channel]];
    }
    return self;
}

// Singleton to create Pubhub conntection shared by both view Controllers.

+ (instancetype)sharedConnection {
    static dispatch_once_t once;
    static id sharedConnection;
    dispatch_once(&once, ^{
        sharedConnection = [[self alloc] init];
    });
    return sharedConnection;
}

@end
