//
//  AppDelegate.h
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/13/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "PNImports.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,PNDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

