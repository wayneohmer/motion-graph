//
//  sharedPubnubChannel.h
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/17/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNImports.h"

// Singleton to create Pubhub conntection shared by the appDelegate and both view Controllers.

@interface SharedPubnubConnection : NSObject

@property (nonatomic,strong) PNChannel *channel;
@property (nonatomic,assign) BOOL isConnected;

+ (instancetype)sharedConnection;

@end
