/*
 Loosely based on Apple's Accelerometer graph sample code.  Adapted by Wayne Ohmer
 Abstract: Displays a graph of CMMotion output. This class uses Core Animation techniques to avoid needing to render the entire graph every update.
*/

#import "GraphView.h"

// The GraphView class creates static segments of the full graph that move accross to the screen to create the illusion of constant updating.

//segment width must be an even integer
NSUInteger const segmentWidth = 20;

#pragma mark - GraphViewSegment

// The GraphViewSegment manages up to [SegmentWidth] accelerometer values and a CALayer that draws
// the segment of the graph that those values represent.

@interface GraphViewSegment : NSObject

@property(nonatomic,readonly) CALayer *layer;
@property(nonatomic,strong) NSMutableArray *motionData;
@property(nonatomic,assign) NSUInteger motionDataIndex;

@end

@interface GraphViewSegment()

@property (nonatomic,strong) CALayer *layer;
@property (nonatomic,assign) CGFloat masterGraphHeight;

@end

@implementation GraphViewSegment

- (instancetype)initWithHeight:(CGFloat)height {
    
    if (self = [super init]) {
        _layer = [[CALayer alloc] init];
        // the layer will call our -drawLayer:inContext: method to provide content
        _layer.delegate = self;
        _layer.bounds = CGRectMake(0.0, 0.0, segmentWidth, height);
        // Disable blending as this layer consists of non-transperant content.
        _layer.opaque = YES;
        _masterGraphHeight = height;
        // motionDataIndex represents how many slots are left to be filled in the graph, which is also +1 compared to the array index that a new entry will be added
        _motionDataIndex = segmentWidth+1;
        _motionData = [[NSMutableArray alloc] initWithCapacity:33];
        //Fill motionData array with null objects so it can be filled in backwards. This saves us from running through it backwards to draw the graph lines
        for (NSUInteger i=0;i<=segmentWidth;i++){
            [_motionData addObject:[NSNull null]];
        }
    }
    return self;
}

- (void)drawGridLinesInContext:(CGContextRef)context{
    
    //changing orientation of the device changes the graph origin(bug?). Must reset to 0,0 here to compensate.
    CGContextTranslateCTM(context,0.0,0.0);
    // I still avoid doing math inside for loops, So here is a variable for the space between gridlines.
    CGFloat interval = self.masterGraphHeight/maxAcceleration;
    for (CGFloat y = 0; y <= self.masterGraphHeight; y += interval){
        CGContextMoveToPoint(context, 0, y);
        CGContextAddLineToPoint(context,segmentWidth, y);
    }
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextStrokePath(context);
}

- (void)addPointToGraphWithMotion:(CMDeviceMotion *)motionData {
    // If this segment is not full, then we add a new motion object to motionData. Doing it backwards here so we do not have to loop through backwards to draw the data lines.
    if (self.motionDataIndex > 0){
        self.motionDataIndex--;
        // And inform Core Animation to redraw the layer.
        [self.motionData replaceObjectAtIndex:self.motionDataIndex withObject:motionData];

        [self.layer setNeedsDisplay];
    }
}

- (void)drawLayer:(CALayer*)layer inContext:(CGContextRef)context
{
    // Fill in the background
    CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextFillRect(context, self.layer.bounds);
    
    [self drawGridLinesInContext:context];
    
    //set y origin to the bottom of the graph. This makes y negative because y value is backwards to graphing.
    CGContextTranslateCTM(context, 0.0,self.masterGraphHeight-2);
    
    // Draw the graph
    CGPoint lines[segmentWidth*2];

    for (NSUInteger i = 0; i < segmentWidth; ++i) {
        CMDeviceMotion *thisMotion1 = [self.motionData objectAtIndex:i];
        CMDeviceMotion *thisMotion2 = [self.motionData objectAtIndex:i+1];
        CMAcceleration acceleration1,acceleration2;
        //Need to make sure thisMotion is not null before getting data.
        if ([thisMotion1 isKindOfClass:[CMDeviceMotion class]]) {
            acceleration1 = thisMotion1.userAcceleration;
            lines[i*2].y = -((fabs(acceleration1.x) + fabs(acceleration1.y) + fabs(acceleration1.z))/maxAcceleration)*self.masterGraphHeight;
        }else{
            lines[i*2].y = 0.0;
        }
        if ([thisMotion2 isKindOfClass:[CMDeviceMotion class]]) {
            acceleration2 = thisMotion2.userAcceleration;
            lines[i*2+1].y = -((fabs(acceleration2.x) + fabs(acceleration2.y) + fabs(acceleration2.z))/maxAcceleration)*self.masterGraphHeight;
        }else{
            lines[i*2+1].y = 0.0;
        }
        lines[i*2].x = i;
        lines[i*2+1].x = i + 1;
    }

    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    CGContextStrokeLineSegments(context, lines, segmentWidth*2);
}

- (id)actionForLayer:(CALayer *)layer forKey :(NSString *)key {
    // We disable all actions for the layer, so no content cross fades, no implicit animation on moves, etc.
    return [NSNull null];
}

@end

#pragma mark - GraphTextView

// We use a seperate view to draw the text for the graph so that we can layer the segment layers below it
// which gives the illusion that the numbers are drawn over the graph, and hides the fact that the graph drawing
// for each segment is incomplete until the segment is filled.

@interface GraphTextView : UIView

@property (nonatomic,assign) CGFloat masterViewHeight;

@end

// View to display Y axis numbers
@implementation GraphTextView

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();

    // Fill in the background
    CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextFillRect(context, self.bounds);

    // Draw the text. I left a lot of static numbers here because it all has to do with the font. Could be paramaterized
    UIFont *scaleFont = [UIFont systemFontOfSize:12.0];
    [[UIColor whiteColor] set];
    CGFloat interval = self.frame.size.height/(CGFloat)maxAcceleration;
    NSDictionary *textAttributes = @{NSFontAttributeName:scaleFont,NSForegroundColorAttributeName:[UIColor whiteColor]};
    NSString *axisText;
    
    [@"0" drawInRect:CGRectMake(2.0,self.frame.size.height-14, segmentWidth, 16) withAttributes:textAttributes];
    for (NSUInteger scaleIndex = 0; scaleIndex < maxAcceleration; scaleIndex++){
        axisText = [NSString stringWithFormat:@"%@",@(maxAcceleration-scaleIndex)];
        [axisText drawInRect:CGRectMake(2.0, (scaleIndex*interval)-14, segmentWidth, 16) withAttributes:textAttributes];
    }
    // Draw grid lines for just the text view.
    for (CGFloat y = 0; y <= self.bounds.size.height; y += (self.bounds.size.height/maxAcceleration)){
        CGContextMoveToPoint(context, 0, y);
        CGContextAddLineToPoint(context,self.bounds.size.width, y);
    }
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextStrokePath(context);
}

@end

#pragma mark - GridView

//Separate view for grid lines on the graph. Needed because it needs to be redrawn after device orientaion change.
//If we just draw the lines in the GraphView, the line thickness changes when the device orientaion changes.
@interface GridView: UIView

@property (nonatomic,assign) CGFloat masterViewHeight;

@end

@implementation GridView

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextFillRect(context, self.bounds);

    CGFloat width = self.bounds.size.width;
    // Draw the grid lines
    for (CGFloat y = 0; y <= self.bounds.size.height; y += (self.bounds.size.height/maxAcceleration)) {
        CGContextMoveToPoint(context, 0, y);
        CGContextAddLineToPoint(context,width, y);
    }
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextStrokePath(context);
}

@end

#pragma mark - GraphView

// Finally the actual GraphView class. This class handles the public interface as well as arranging
// the subviews and sublayers to produce the intended effect.

@interface GraphView()

@property (nonatomic,strong) NSMutableArray *segments;
@property (nonatomic,strong) GraphViewSegment *currentGraphSegement;
@property (nonatomic,strong) GraphTextView *textView;
@property (nonatomic,strong) GridView *gridView;

@end

@implementation GraphView

- (void)makeTextView {
    // Create the text view and add it as a subview.
    self.textView = [[GraphTextView alloc] initWithFrame:CGRectMake(0.0, 0.0,segmentWidth, self.frame.size.height)];
    self.textView.masterViewHeight = self.frame.size.height;
    [self addSubview:self.textView];
    // Create a mutable array to store segments, which is required by -addSegment. should be done in init...
    self.segments = [[NSMutableArray alloc] init];
    self.currentGraphSegement = [self addSegment];

}

- (void)addAccelerationPoint:(CMDeviceMotion *)motionData {
    // First, add the new acceleration value to the current segment

    [self.currentGraphSegement addPointToGraphWithMotion:motionData];
    //If segment is full, make another one and add the same point to it. This makes the graph continious.
    // motionDataIndex ==  is "full" because we add the data backwards
    if (self.currentGraphSegement.motionDataIndex == 0){
        self.currentGraphSegement = [self addSegment];
        [self.currentGraphSegement addPointToGraphWithMotion:motionData];
    }
    // After adding a new data point, we need to advance the x-position of all the segment layers by 1 to
    // create the illusion that the graph is advancing.
    for (GraphViewSegment *segment in self.segments){
        CGPoint position = segment.layer.position;
        position.x += 1.0;
        //sometimes segments get out of place on rotation.
        position.y = self.frame.size.height/2;
        segment.layer.position = position;
    }
}

// The initial position of a segment that is meant to be displayed on the left side of the graph.

- (GraphViewSegment *)addSegment {
    // See if last segment layer is visible. Remove if not.
    // I do not reuse segements because they may have been created in a different orientation.
    GraphViewSegment *last = [self.segments lastObject];
    if (!CGRectIntersectsRect(self.layer.frame, last.layer.frame)){
        [last.layer removeFromSuperlayer];
        [self.segments removeLastObject];
    }
    // Create a new segment and add it to the begining of the segments array.
    GraphViewSegment *segment = [[GraphViewSegment alloc] initWithHeight:self.frame.size.height];
    segment.masterGraphHeight = self.bounds.size.height;
    [self.segments insertObject:segment atIndex:0];

    // Ensure that newly added segment layers are placed below the text view's layer so that the text view
    // is hidden until it moves past.
    [self.layer insertSublayer:segment.layer below:self.textView.layer];
    // position new layer in the center of text View
    segment.layer.position = CGPointMake(self.textView.bounds.size.width/2,self.textView.bounds.size.height/2);

    return segment;
}

- (void) clearGraph {
    //Hide layers. Removing them causes autorelease pool to crash. They are removed above.
    for (CALayer *thisLayer in self.layer.sublayers) {
         thisLayer.hidden = YES;
    }
    [self.textView removeFromSuperview];
    [self.gridView removeFromSuperview];
}

//done after graph is cleared so it can be used again. Resets sizes based on new current frame
- (void) reinitializeGraph {
    self.textView = [[GraphTextView alloc] initWithFrame:CGRectMake(0.0, 0.0, segmentWidth, self.frame.size.height)];
    self.textView.masterViewHeight = self.frame.size.height;
    [self addSubview:self.textView];
    self.gridView = [[GridView alloc] initWithFrame:CGRectMake(segmentWidth, 0.0, self.frame.size.width -segmentWidth, self.frame.size.height)];
    [self addSubview:self.gridView];
    [self sendSubviewToBack:self.gridView];
}

// The graph view itself exists only to draw the background and gridlines. All other content is drawn either into
// the GraphTextView or into a layer managed by a GraphViewSegment.
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    // Fill in the background
    CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextFillRect(context, self.bounds);
    self.gridView = [[GridView alloc] initWithFrame:CGRectMake(segmentWidth, 0.0, self.frame.size.width -segmentWidth, self.frame.size.height)];
    [self addSubview:self.gridView];
    [self makeTextView];
}

@end
