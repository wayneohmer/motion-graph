//
//  FirstViewController.m
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/13/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import "GraphGaugeViewController.h"

NSTimeInterval static motionUpdateInterval = .1;
NSInteger static  maxTotalAcceleration = 500;
NSInteger static gaugeWidthBuffer = 30;
NSUInteger static maxArraySize = 1200;

@interface GraphGaugeViewController ()

@property (nonatomic,strong) CMMotionManager *motionManager;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *needleXConstraint;
@property (nonatomic,weak) IBOutlet UIImageView *needleImageView;
@property (nonatomic,weak) IBOutlet UISwitch *gaugeSwitch;
@property (nonatomic,weak) IBOutlet UILabel *gaugeMaxLabel;
@property (nonatomic,weak) IBOutlet UIButton *pauseButton;
@property (nonatomic,weak) IBOutlet UIButton *saveButton;
@property (nonatomic,assign) BOOL pauseFlag;
//array for saving to pubHub
@property (nonatomic,strong) NSMutableArray *movementHistoryData;
@property (nonatomic,strong) NSMutableArray *motionData;
//array for rebuilding graph
@property (nonatomic,assign) double runningTotal;
@property (nonatomic,assign) CGFloat gaugeWidthRange;
@property (nonatomic,weak) IBOutlet GraphView *graphView;
@property (nonatomic,strong) SharedPubnubConnection *pubHubChannel;

@end

@implementation GraphGaugeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.deviceMotionUpdateInterval = motionUpdateInterval;
    [self createPubnubObservers];
    [self.motionManager  startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^( CMDeviceMotion  *motionData, NSError *error) {
                                                 [self outputAccelertionData:motionData];
                                                 if(error){
                                                     NSLog(@"startDeviceMotion Error");
                                                 }
                                             }];
    self.gaugeWidthRange = self.view.bounds.size.width - gaugeWidthBuffer;
    self.runningTotal = 0.0;
    self.gaugeSwitch.on = NO;
    self.pauseFlag = NO;
    self.movementHistoryData = [[NSMutableArray alloc] init];
    self.motionData = [[NSMutableArray alloc] init];
    self.pubHubChannel = [SharedPubnubConnection sharedConnection];
    if (self.pubHubChannel.isConnected){
        self.saveButton.enabled = TRUE;
    }else{
        self.saveButton.enabled = FALSE;
    }
    // Timer to disable save button when pubnub is not connected. The connection observer is in the app delegate
    [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(checkSaveButton:) userInfo:nil repeats:YES];
    [self.view layoutIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated {
   self.saveButton.enabled = self.pubHubChannel.isConnected;
}

- (void) viewDidAppear:(BOOL)animated {
    //This is needed in case orientation changed while in the other view.
    [self.graphView clearGraph];
    [self.graphView reinitializeGraph];
    for (CMDeviceMotion *motionData in self.motionData) {
        [self.graphView addAccelerationPoint:motionData];
    }
    self.gaugeWidthRange = self.view.bounds.size.width - gaugeWidthBuffer;
}

- (void) checkSaveButton:(NSTimer *)timer {
    self.saveButton.enabled = self.pubHubChannel.isConnected;
}

-(void)outputAccelertionData:(CMDeviceMotion *)motionData {
 
    if (!self.pauseFlag) {
        CMAcceleration acceleration = motionData.userAcceleration;
        double total,animationMultiplier;
        //ignoring direction here. Getting total acceleration
        total = fabs(acceleration.x)+fabs(acceleration.y)+fabs(acceleration.z);
        //converting to NSNumber so it can be put in to an NSMutable array and saved to Pubhub
        NSNumber *movementPoint = [NSNumber numberWithDouble:total];
        [self.movementHistoryData addObject:movementPoint];
        self.runningTotal += total;
        if (self.gaugeSwitch.on) {
            animationMultiplier = self.runningTotal/maxTotalAcceleration;
            self.gaugeMaxLabel.text = [NSString stringWithFormat:@"%@G",@(maxTotalAcceleration)];
        }else{
            animationMultiplier = total/maxAcceleration;
            self.gaugeMaxLabel.text = [NSString stringWithFormat:@"%@G",@(maxAcceleration)];
        }
        //Peg needle so it does not go off screen
        if (animationMultiplier > 1) {
            animationMultiplier = 1.0;
        }
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:motionUpdateInterval
                         animations:^{
                             self.needleXConstraint.constant = ((CGFloat)animationMultiplier)*self.gaugeWidthRange;
                             [self.view layoutIfNeeded];
                         }];
        //add point to graph
        [self.graphView addAccelerationPoint:motionData];
        //add point to array so we can recreate graph if needed
        [self.motionData addObject:motionData];
        // don't let arrays get too big.
        if ([self.motionData count] > maxArraySize) {
            [self.motionData removeObjectAtIndex:0];
        }
        if ([self.movementHistoryData count] > maxArraySize) {
            [self.movementHistoryData removeObjectAtIndex:0];
        }
    }
 }

- (void) createPubnubObservers {
    [[PNObservationCenter defaultCenter] addMessageProcessingObserver:self
                                                            withBlock:^(PNMessageState state, id data) {
                                                                switch (state) {
                                                                    case PNMessageSending:
                                                                        break;
                                                                    case PNMessageSent:
                                                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

                                                                        break;
                                                                    case PNMessageSendingError:{
                                                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Save Failed",nil) message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                                        [alert show];
                                                                        NSLog(@"Sending Message Error");
                                                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                                        break;
                                                                    }
                                                                }
                                                            }];
}

#pragma mark - Button Action Methods

- (IBAction)PauseButtonTouch:(UIButton *)sender {
    if (self.pauseFlag) {
        self.pauseFlag = NO;
        [self.pauseButton setTitle:NSLocalizedString(@"Pause",nil) forState:UIControlStateNormal];
    }else{
        self.pauseFlag = YES;
        [self.pauseButton setTitle:NSLocalizedString(@"Resume",nil) forState:UIControlStateNormal];
    }
}

- (IBAction)resetButtonTouch:(UIButton *)sender {
    self.runningTotal = 0.0;
    [self.graphView clearGraph];
    [self.graphView reinitializeGraph];
    [self.motionData removeAllObjects];
    [self.movementHistoryData removeAllObjects];
}

- (IBAction)saveButtonTouch:(UIButton *)sender {
    //save array of NSNumbers to pubhub.  
    NSArray *messageArray = [self.movementHistoryData copy];
    [PubNub sendMessage:@{@"message":messageArray, @"sender":@"Movement-app"} toChannel:self.pubHubChannel.channel];
    [self PauseButtonTouch:sender];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

#pragma mark - Rotation Methods

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    //clear before orientation so we do not see artifacts in new orientation
    [self.graphView clearGraph];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    //redraw graph after new orientaion has been established.
    [self.graphView reinitializeGraph];

    for (CMDeviceMotion *motionData in self.motionData) {
       [self.graphView addAccelerationPoint:motionData];
    }
    self.gaugeWidthRange = self.view.bounds.size.width - gaugeWidthBuffer;
}

@end
