//
//  SecondViewController.m
//  Movement Code Test
//
//  Created by Wayne Ohmer on 1/13/15.
//  Copyright (c) 2015 Wayne Ohmer. All rights reserved.
//

#import "HistoryViewController.h"

@interface HistoryViewController ()
@property (weak, nonatomic) IBOutlet UIButton *getHistoryButton;
@property (nonatomic,strong) SharedPubnubConnection *pubnubConnection;
@property (nonatomic,strong) NSArray *historyArray;
@property (weak, nonatomic) IBOutlet UITableView *historyTable;
@property (weak, nonatomic) IBOutlet UILabel *pubHubStatusLabel;

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createPubnubObservers];
    self.historyArray = @[];
    self.pubnubConnection = [SharedPubnubConnection sharedConnection];
    //timer to inform user of Pubhub connection status. The connection observer is in the appdelegate
    [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(updateNetworkStatus:) userInfo:nil repeats:YES];

     // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    self.getHistoryButton.enabled = self.pubnubConnection.isConnected;
    [self updateNetworkStatus:nil];
}

- (void) updateNetworkStatus:(NSTimer *)timer {
    self.getHistoryButton.enabled = self.pubnubConnection.isConnected;
    if (self.pubnubConnection.isConnected) {
        self.pubHubStatusLabel.text = NSLocalizedString(@"Pubhub Connected",nil);
    }else{
        self.pubHubStatusLabel.text = NSLocalizedString(@"Pubhub Disconnected",nil);
    }
}

- (IBAction)getHistTouch:(UIButton *)sender {
    //ask for all messages in this channel. Data is recieved in the message history observer. 
    [PubNub requestHistoryForChannel:self.pubnubConnection.channel from:nil to:nil limit:50 reverseHistory:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void) createPubnubObservers {
//pubnum message history observer. This is where we get the message data.
[[PNObservationCenter defaultCenter] addMessageHistoryProcessingObserver:self
                                                                   withBlock:^(NSArray *messages,PNChannel *channel,PNDate *start,PNDate *end, PNError *error){
                                                                       if (error) {
                                                                           NSLog(@"Get Hisotry Error");
                                                                           [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

                                                                       }else{
                                                                           dispatch_async(dispatch_get_main_queue(), ^ {
                                                                               self.historyArray = [[messages reverseObjectEnumerator] allObjects ];
                                                                               [self.historyTable reloadData];
                                                                               [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

                                                                           });
                                                                       }
                                                                   }];
}

#pragma mark - Table Delegate Methods

//simple table that displays basic information about data retrieved from pubhub

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HistoryTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryTableCell"];
    if (cell == nil){
        cell = [[HistoryTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HistoryTableCell"];
    }
    PNMessage *thisMessage = [self.historyArray objectAtIndex:indexPath.row];
    if ([thisMessage.message isKindOfClass:[NSDictionary class]]){
        id messageObject = thisMessage.message[@"message"];
        if ([thisMessage.message[@"sender"] isEqualToString:@"Movement-app"] && [messageObject isKindOfClass:[NSArray class]] ) {
            double totalAcceleration = 0.0;
            for (NSNumber *thisNumber in messageObject) {
                totalAcceleration += [thisNumber doubleValue];
            }
            cell.historyCellLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Data Points: %@ Total:%2.2fG",nil),@([messageObject count]),totalAcceleration];
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.historyArray count];
}

@end
