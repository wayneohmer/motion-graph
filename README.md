## Metal Toad code test ##

Here is my approach to solving the problem stated under "Problem Statement" below.  On launch, the app begins monitoring motion using CMMotionManager.  Every 0.1 seconds a needle at the bottom moves to indicate the total acceleration of the device during that interval, which can be anywhere from 0 to 10G.  It also updates a real time graph of the same data. 

The app also keeps a running total of acceleration amounts.  A switch allows the user to change the needle to display the "running total" or "instantaneous" acceleration.  The running total is capped at 500G.  

A "save" button allows the user to turn data points into a PubNub message that is then sent to a PubNub channel that I have created.  A second "History" view allows the user to retrieve messages from the PubNub channel. The total number of data points and a grand total are calculated from these messages and put in a UITableView, for the purpose of verifying that the correct data was retrieved.  

A pause button stops data from being monitored until resume is touched. The save button also pauses data monitoring.  

A reset button clears out all saved data and clears the graph. 

##Technical Features:##

The app works in portrait and landscape orientations with different layouts. I used size classes and constraints in Interface Builder to achieve this.  

I use "userAcceleration" from the CMMotionManager because it filters out the effects of gravity.

The needle is controlled by animating an NSLayoutConstraint change on the UIImageView. 

I use the PubNub connection state observer in the app delegate to constantly communicate the connection state to both view controllers. If the connection goes away, the "save" button and the "get history" button are disabled and a label in the history view is updated with the connection state. This is reversed when the connection comes back. The timing is based on the state observer and I have noticed a bit of a delay in reporting the connection coming back while the app is running.  Separate timers in each view controller monitor the connection state. 

I used the "Messages" and "History" APIs from Pubnub to store data. I send an NSArray of NSNumbers in the message. 

The real time graph is loosely based on the Apple accelerometer graph sample code(GraphView).  I heavily modified it to make it smaller, more modern, and more flexible. It draws small sections of the graph behinds the scenes and moves them across the screen. This prevents the need to redraw the entire graph every time a point is added.  

**Problem statement:**

Write a native app (iOS or Android) that consumes acceleration/gyro data and
draws a realtime graph which is meant to represent the user's movement.
The gauge represent the realtime accelerometer values with an option to represent
cumulative movement since the app first started.
The needle of the gauge must be as realtime as possible and show immediate
change in movement as well as accumulated total movement since the
start of the application.  This application should use the most efficient
method possible to upload the captured data to a free account on

http://PubNub.com which you can create with your email address...

User interface: display a gauge with units of measure and an indicator.
Think of a fuel gauge in a car with “empty" on the left side, “full" on
the right and a needle which represents movement to rotate from empty to full

(http://www.4freephotos.com/Empty_fuel_gauge-limage-a9c0b89591c96a0aa610a186ccfe85d1.html# )

Note: You may only use the http://pubnub.com sdk as a 3rd party library.

The app should be delivered as a working project that we can load, and
monitor the ram or battery performance in Android Studio (if applying for
an Android position) or on Xcode (if applying for an iOS position).

Provide unit tests where appropriate.